package com.example.taskscheduler;

import com.google.gson.annotations.SerializedName;

import java.util.Dictionary;
import java.util.Objects;

/**
 * Represents a task entity within the application.
 */
public class Task {

    @SerializedName("task_name")
    private String task_name;

    @SerializedName("start_time")
    private String start_time;

    @SerializedName("end_time")
    private String end_time;

    private boolean status;

    public Task(String startTime, String endTime, String name, boolean status) {
        this.start_time = startTime;
        this.end_time = endTime;
        this.task_name = name;
        this.status = status;
    }



    // Getters and setters
    public String getTask_name(){return task_name;}
    public void setTask_name(String task_name){this.task_name = task_name;}
    public String getStartTime() {
        return start_time;
    }

    public void setStartTime(String startTime)
    {
        this.start_time = startTime;
    }

    public String getEndTime() {
        return end_time;
    }

    public void setEndTime(String endTime) {
        this.end_time = endTime;
    }

    public boolean getStatus()
    {
        return this.status;
    }
    public void setStatus(boolean status){this.status = status;}

    public String getName() {
        return task_name;
    }

    public String getTimeStr() {
        return this.start_time + "-" + this.end_time;
    }
}