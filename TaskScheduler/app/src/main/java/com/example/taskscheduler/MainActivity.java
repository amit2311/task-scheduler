package com.example.taskscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button SignupBtn;
    Button LoginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SignupBtn = findViewById(R.id.signup_btn_main);
        LoginBtn = findViewById(R.id.login_btn_main);
        SignupBtn.setOnClickListener(this);
        LoginBtn.setOnClickListener(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if(v.getId() == SignupBtn.getId())
        {
            Toast.makeText(this, "Sign Up", Toast.LENGTH_SHORT).show();
            intent = new Intent(this, SignupActivity.class);
        }
        else if (v.getId() == LoginBtn.getId())
        {
            Toast.makeText(this, "Login", Toast.LENGTH_SHORT).show();
            intent = new Intent(this, LoginActivity.class);
        }
        else
        {
            return;
        }
        startActivity(intent);
    }
}