package com.example.taskscheduler;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static void showDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true); // Allow user to dismiss the dialog
        builder.show();
    }
    public static boolean checkPassword(String password) {
        // Check for at least one capital letter
        Pattern capitalPattern = Pattern.compile("[A-Z]");
        Matcher capitalMatcher = capitalPattern.matcher(password);

        // Check for at least one small letter
        Pattern smallPattern = Pattern.compile("[a-z]");
        Matcher smallMatcher = smallPattern.matcher(password);

        // Check for at least one digit
        Pattern digitPattern = Pattern.compile("[0-9]");
        Matcher digitMatcher = digitPattern.matcher(password);

        // Check for at least one special character
        Pattern specialPattern = Pattern.compile("[^A-Za-z0-9]");
        Matcher specialMatcher = specialPattern.matcher(password);

        // Check if all conditions are satisfied
        return password.length() >= 8 && capitalMatcher.find() && smallMatcher.find() && digitMatcher.find() && specialMatcher.find();
    }

    public static String getCurrentDate()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

}
