package com.example.taskscheduler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;

public class JoinWorkplaceActivity extends AppCompatActivity implements View.OnClickListener{

    ArrayList<Workplace> workplaces;
    RecyclerView RV_workplaces;
    Button SubmitBtn;
    EditText ET_password;
    WorkplaceAdapter adapter;
    String Username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_workplace);

        SubmitBtn = findViewById(R.id.submit_btn_join);
        SubmitBtn.setOnClickListener(this);
        ET_password = findViewById(R.id.workplace_password_input_join);
        try {
            workplaces = Communicator.getWorkplaces(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        setWorkplaceList();
        Username = getPassedUsername();
    }
    /**
     * Sets up the RecyclerView to display the list of workplaces.
     * This method initializes the RecyclerView, sets its layout manager,
     * and attaches an adapter to populate the list with workplace data.
     */
    private void setWorkplaceList() {
        // Find the RecyclerView in the layout
        RV_workplaces = findViewById(R.id.rv_workplaces);

        // Set layout manager for RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RV_workplaces.setLayoutManager(layoutManager);

        // Initialize adapter with the list of workplaces and set it to RecyclerView
        adapter = new WorkplaceAdapter(workplaces);
        RV_workplaces.setAdapter(adapter);
    }


    /**
     * Retrieves the username passed from the previous activity.
     * @return The username passed from the previous activity, or null if not found.
     */
    String getPassedUsername()
    {
        Intent intent = getIntent();
        return intent.getStringExtra("username");
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == SubmitBtn.getId())
        {
            Workplace submitted_workplace = adapter.getSelectedItem();
            String password = ET_password.getText().toString();
            try {
                if(Communicator.joinWorkplace(this, Username, submitted_workplace.getName(), password))
                {
                    Intent intent = new Intent(this, WorkplaceTaskListActivity.class);
                    intent.putExtra("username", Username);
                    intent.putExtra("workplace_name", submitted_workplace.getName());
                    startActivity(intent);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
    }
}