package com.example.taskscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, TextToSpeech.OnInitListener {

    private static final int REQ_CODE_SPEECH_INPUT = 100;
    private TextToSpeech textToSpeech;
    EditText usernameEntry;
    EditText passwordEntry;
    Button LoginBtn;
    SharedPreferences sharedPreferences;
    ImageView tts_iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //initialize text to speech
        textToSpeech = new TextToSpeech(this, this);

        usernameEntry = findViewById(R.id.username_input_login);
        passwordEntry = findViewById(R.id.password_input_login);
        LoginBtn = findViewById(R.id.login_btn_login);
        LoginBtn.setOnClickListener(this);
        tts_iv = findViewById(R.id.TTS_iv);
        tts_iv.setOnClickListener(this);

        // Initialize SharedPreferences
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Retrieve the saved username and set it in the EditText
        String savedUsername = sharedPreferences.getString("username", "");
        usernameEntry.setText(savedUsername);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == LoginBtn.getId()) {
            String username = usernameEntry.getText().toString();
            String password = passwordEntry.getText().toString();
            try {
                if (Communicator.logIn(this, username, password)) {
                    String workplaceName = Communicator.getUsersWorkplace(this, username);
                    Intent intent;
                    if (workplaceName == null) {
                        intent = new Intent(this, WorkplaceMenuActivity.class);
                    } else {
                        intent = new Intent(this, WorkplaceTaskListActivity.class);
                        intent.putExtra("workplace_name", workplaceName);
                    }
                    intent.putExtra("username", username);
                    startActivity(intent);

                    // Save the username in SharedPreferences
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("username", username);
                    editor.apply();
                }
            } catch (IOException e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        else if (v.getId() == tts_iv.getId()) {
            // Handle mic image view click
            startSpeechToText();
        }
    }

    @Override
    public void onInit(int status) {

    }
    private void startSpeechToText() {
        // Start speech recognition
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak now...");

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Speech recognition not supported", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if (result != null && result.size() > 0) {
                    // Set the speech recognition result in the username EditText
                    usernameEntry.setText(result.get(0));
                }
            }
        }
    }
}