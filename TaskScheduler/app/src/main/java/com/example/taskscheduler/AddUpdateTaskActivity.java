package com.example.taskscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;

public class AddUpdateTaskActivity extends AppCompatActivity implements View.OnClickListener{

    final int ADD_CODE = 78;
    final int UPDATE_CODE = 79;
    EditText task_nameET;
    Button end_time, start_time, submitBtn, dateBtn;

    String end_time_result, start_time_result, date_result;
    boolean end_picked = false, start_picked = false, datePicked = false, isAdd = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update_task);
        task_nameET = findViewById(R.id.task_name);
        end_time = findViewById(R.id.end_time_btn);
        start_time = findViewById(R.id.start_time_btn);
        submitBtn = findViewById(R.id.submit_btn_add_task);
        dateBtn = findViewById(R.id.date_btn);
        dateBtn.setOnClickListener(this);
        end_time.setOnClickListener(this);
        start_time.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        Intent intent = getIntent();

        //made to check if the activity was open for adding or updating
        isAdd = intent.getBooleanExtra("isAdd", true);
    }

    @Override
    public void onClick(View v) {
        // Calendar setup
        Calendar systemCalendar = Calendar.getInstance();

        if (v == start_time || v == end_time) {
            // Time picker dialog setup
            int hour = systemCalendar.get(Calendar.HOUR_OF_DAY);
            int minute = systemCalendar.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new DateSetListener(v == start_time), hour, minute, true);
            timePickerDialog.show();
        } else if (v == dateBtn) {
            // Date picker dialog setup
            int year = systemCalendar.get(Calendar.YEAR);
            int month = systemCalendar.get(Calendar.MONTH);
            int dayOfMonth = systemCalendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year1, monthOfYear, dayOfMonth1) -> {
                // Format date
                String formattedMonth = String.format(Locale.getDefault(), "%02d", monthOfYear + 1);
                String formattedDayOfMonth = String.format(Locale.getDefault(), "%02d", dayOfMonth1);
                String formattedDate = year1 + "-" + formattedMonth + "-" + formattedDayOfMonth;
                date_result = formattedDate;
                datePicked = true;
            }, year, month, dayOfMonth);
            datePickerDialog.show();
        }
        // Submit button handler
        else if (v == submitBtn) {
            String task_name = task_nameET.getText().toString();

            // Check if necessary parameters are filled
            if (!start_picked || !end_picked || !datePicked || task_name.isEmpty()) {
                Toast.makeText(this, "Please fill all necessary parameters", Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent();
                intent.putExtra("end_time", end_time_result);
                intent.putExtra("start_time", start_time_result);
                intent.putExtra("date", date_result);
                intent.putExtra("task_name", task_name);
                if (isAdd) {
                    setResult(ADD_CODE, intent);
                    finish();
                } else {
                    setResult(UPDATE_CODE, intent);
                    finish();
                }
            }
        }
    }


    /**
     * A listener class to handle time selection events from a TimePickerDialog.
     */
    public class DateSetListener implements TimePickerDialog.OnTimeSetListener
    {
        private final boolean is_start;
        public DateSetListener(boolean is_start)
        {
            this.is_start = is_start;
        }

        /**
         * Called when a time is set in the TimePickerDialog.
         * @param view The TimePicker view.
         * @param hourOfDay The selected hour.
         * @param minute The selected minute.
         */
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Format hour and minute to ensure they are displayed with leading zeros if needed
            String formattedHour = String.format(Locale.getDefault(), "%02d", hourOfDay);
            String formattedMinute = String.format(Locale.getDefault(), "%02d", minute);

            // Construct the time string in HH:MM format
            String time = formattedHour + ":" + formattedMinute;

            // Update the appropriate result variable based on whether it's start or end time
            if (is_start) {
                start_time_result = time;
                start_picked = true;
            } else {
                end_time_result = time;
                end_picked = true;
            }
        }

    }
}