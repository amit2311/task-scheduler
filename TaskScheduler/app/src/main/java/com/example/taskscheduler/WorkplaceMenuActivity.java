package com.example.taskscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Activity for displaying the menu options for a workplace.
 */
public class WorkplaceMenuActivity extends AppCompatActivity implements View.OnClickListener{

    Button CreateBtn;
    Button JoinBtn;
    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workplace_menu);
        CreateBtn = findViewById(R.id.create_btn_workplace);
        JoinBtn = findViewById(R.id.join_btn_workplace);
        CreateBtn.setOnClickListener(this);
        JoinBtn.setOnClickListener(this);
        username = getPassedUsername();
    }

    /**
     * Retrieves the username passed from the previous activity.
     * @return The username passed from the previous activity, or null if not found.
     */
    String getPassedUsername()
    {
        Intent intent = getIntent();
        return intent.getStringExtra("username");
    }

    @Override
    public void onClick(View v)
    {
        Intent intent;

        if(v.getId() == CreateBtn.getId())
        {
            intent = new Intent(this, CreateWorkplaceActivity.class);
            intent.putExtra("username", username);
            startActivity(intent);
        }
        else if (v.getId() == JoinBtn.getId())
        {

            intent = new Intent(this, JoinWorkplaceActivity.class);
            intent.putExtra("username", username);
            startActivity(intent);
        }
    }
}