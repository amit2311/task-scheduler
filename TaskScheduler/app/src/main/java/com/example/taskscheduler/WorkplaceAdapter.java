package com.example.taskscheduler;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Adapter class for populating a RecyclerView with Workplace items.
 */
public class WorkplaceAdapter extends RecyclerView.Adapter<WorkplaceAdapter.WorkplaceViewHolder>
{
    private int row_index = -1;
    private ArrayList<Workplace> workplaces;

    public WorkplaceAdapter(ArrayList<Workplace> workplaces)
    {
        this.workplaces = workplaces;
    }

    /**
     * ViewHolder class to hold the views for individual task items.
     */
    public static class WorkplaceViewHolder extends RecyclerView.ViewHolder
    {
        public TextView TV_ownerName;
        public TextView TV_workplaceName;
        public LinearLayout background;
        public WorkplaceViewHolder(@NonNull View itemView) {
            super(itemView);
            TV_workplaceName = itemView.findViewById(R.id.textViewWorkplaceName);
            TV_ownerName = itemView.findViewById(R.id.textViewOwnerName);
            background = itemView.findViewById(R.id.workplace_item);
        }
    }

    /**
     * Creates a new WorkplaceViewHolder instance.
     * @param parent The parent ViewGroup into which the new View will be added.
     * @param viewType The type of the new View.
     * @return A new WorkplaceViewHolder instance.
     */
    @NonNull
    @Override
    public WorkplaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View workplaceView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_workplace, parent, false);
        return new WorkplaceViewHolder(workplaceView);
    }

    /**
     * Binds data to the views inside the TaskViewHolder.
     * @param holder The TaskViewHolder instance to bind data to.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull WorkplaceViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Workplace curr = workplaces.get(position);
        holder.TV_ownerName.setText(curr.getOwner_name());
        holder.TV_workplaceName.setText(curr.getName());
        holder.background.setOnClickListener(v -> {
            row_index = position;
            notifyDataSetChanged();
        });

        //paint selected item in blue
        if(position == row_index)
        {
            holder.background.setBackgroundColor(Color.BLUE);
        }
        else
        {
            holder.background.setBackgroundColor(Color.BLACK);
        }


    }

    /**
     * Gets the selected workplace item.
     * @return The selected Workplace object.
     */
    public Workplace getSelectedItem()
    {
        return workplaces.get(row_index);
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     * @return The total number of items in the data set.
     */
    @Override
    public int getItemCount() {
        return workplaces.size();
    }

}
