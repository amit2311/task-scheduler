package com.example.taskscheduler;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Activity for displaying the task list of a workplace and managing tasks.
 */
public class WorkplaceTaskListActivity extends AppCompatActivity implements View.OnClickListener{


    private final int INTERVAL = 1000;
    private final Handler handler = new Handler(Looper.getMainLooper());

    //updater thread for the recyclerView
    private final Runnable task = new Runnable() {
        @Override
        public void run() {
            // Refresh the task list
            refreshTaskList();
            // Schedule the task to run again after INTERVAL milliseconds
            handler.postDelayed(this, INTERVAL);
        }
    };
    final int ADD_TASK_CODE = 78;
    final int UPDATE_TASK_CODE = 79;
    ArrayList<Task> tasks;
    RecyclerView RV_tasks;
    TaskAdapter adapter;
    Button rescheduleBtn;
    Button deleteBtn;
    Button statusBtn;
    Button addBtn;
    Button dateBtn;
    String username;
    String workplace_name;
    String current_date;


    /**
     * Activity result launcher for handling results from activities launched for adding or updating tasks.
     */
    ActivityResultLauncher<Intent> activityLauncher =
            registerForActivityResult(
                    new ActivityResultContracts.StartActivityForResult(),
                    new ActivityResultCallback<ActivityResult>() {
                        /**
                         * Called when the result of the launched activity is available.
                         * @param result The result of the launched activity.
                         */
                        @Override
                        public void onActivityResult(ActivityResult result) {
                            // Check the result code to determine the action
                            if(result.getResultCode() == ADD_TASK_CODE) {
                                // If the result is for adding a task
                                Intent intent = result.getData();
                                if(intent != null) {
                                    // Extract task details from the intent
                                    String task_name = intent.getStringExtra("task_name");
                                    String start_time = intent.getStringExtra("start_time");
                                    String end_time = intent.getStringExtra("end_time");
                                    String date = intent.getStringExtra("date");
                                    try {
                                        // Add the task using Communicator if available
                                        Communicator.addTask(WorkplaceTaskListActivity.this, workplace_name, username, start_time, end_time, task_name, date);
                                    } catch (IOException e) {
                                        // Handle IOException by displaying a toast and throwing a RuntimeException
                                        Toast.makeText(WorkplaceTaskListActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                        throw new RuntimeException(e);
                                    }
                                }
                            }
                            else if(result.getResultCode() == UPDATE_TASK_CODE) {
                                // If the result is for updating a task
                                Intent intent = result.getData();
                                if(intent != null) {
                                    // Extract task details from the intent
                                    String task_name = intent.getStringExtra("task_name");
                                    String start_time = intent.getStringExtra("start_time");
                                    String end_time = intent.getStringExtra("end_time");
                                    String date = intent.getStringExtra("date");
                                    try {
                                        // Delete the existing task and add the updated task if available
                                        if(Communicator.deleteTask(WorkplaceTaskListActivity.this, username, task_name, workplace_name))
                                            Communicator.addTask(WorkplaceTaskListActivity.this, workplace_name, username, start_time, end_time, task_name, date);
                                    } catch (IOException e) {
                                        // Handle IOException by displaying a toast and throwing a RuntimeException
                                        Toast.makeText(WorkplaceTaskListActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                        throw new RuntimeException(e);
                                    }
                                }
                            }
                        }
                    }
            );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workplace_task_list);
        buttonSetup();
        getPassedParameters();


        current_date = Utils.getCurrentDate();
        try {
            tasks = Communicator.getTasksForWorkplace(this, workplace_name, current_date);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        setTaskList();
        handler.postDelayed(task, INTERVAL);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Stop the periodic task when the activity is destroyed to prevent memory leaks
        handler.removeCallbacks(task);
    }



    /**
     * Refreshes the task list by fetching updated tasks from the server.
     */
    private void refreshTaskList() {
        try {
            // Get the updated list of tasks
            tasks = Communicator.getTasksForWorkplace(this, workplace_name, current_date);
            // Update the RecyclerView adapter with the new list of tasks
            adapter.setTasks(tasks);
        } catch (IOException e) {
            Toast.makeText(WorkplaceTaskListActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            throw new RuntimeException(e);
        }
    }

    /**
     * Sets up click listeners for buttons.
     */
    private void buttonSetup()
    {
        rescheduleBtn = findViewById(R.id.reschedule_task_btn);
        rescheduleBtn.setOnClickListener(this);
        deleteBtn = findViewById(R.id.delete_task_btn);
        deleteBtn.setOnClickListener(this);
        addBtn = findViewById(R.id.add_task_btn);
        addBtn.setOnClickListener(this);
        statusBtn = findViewById(R.id.change_status_btn);
        statusBtn.setOnClickListener(this);
        dateBtn = findViewById(R.id.select_date_btn);
        dateBtn.setOnClickListener(this);
    }

    /**
     * Retrieves passed parameters (username and workplace name) from the intent.
     */
    private void getPassedParameters()
    {
        Intent intent = getIntent();
        username = intent.getStringExtra("username");
        workplace_name = intent.getStringExtra("workplace_name");
    }

    /**
     * Sets up the task list RecyclerView.
     */
    private void setTaskList()
    {
        RV_tasks = findViewById(R.id.rv_tasks);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RV_tasks.setLayoutManager(layoutManager);

        adapter = new TaskAdapter(tasks);
        RV_tasks.setAdapter(adapter);
    }

    /**
     * Displays a delete confirmation dialog for the specified task.
     * @param submitted_task The task for which the delete confirmation dialog is displayed.
     */
    private void showDeleteConfirmationDialog(Task submitted_task) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete this task?");
        builder.setPositiveButton("Yes", (dialogInterface, i) -> {
            // Your deletion code here
            try {
                if(Communicator.deleteTask(WorkplaceTaskListActivity.this, username, submitted_task.getName(), workplace_name)) {
                    Toast.makeText(WorkplaceTaskListActivity.this, "Task Deleted Successfully", Toast.LENGTH_LONG).show();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v)
    {
        if(v == addBtn)
        {
            try
            {
                Intent intent = new Intent(WorkplaceTaskListActivity.this, AddUpdateTaskActivity.class);
                intent.putExtra("isAdd", true);
                activityLauncher.launch(intent);


            }
            catch (Exception e)
            {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        else if(v.getId() == deleteBtn.getId()) {
            try {
                Task submitted_task = adapter.getSelectedItem();
                showDeleteConfirmationDialog(submitted_task);
            }
            catch (Exception e)
            {
                Toast.makeText(this, "Pick a task!", Toast.LENGTH_LONG).show();
            }
        }
        else if(v.getId() == dateBtn.getId()) {
            // Get the current date
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

            // Create a DatePickerDialog and set the listener
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year1, monthOfYear, dayOfMonth1) -> {
                // Update the date result variable
                current_date = String.format(Locale.getDefault(), "%04d-%02d-%02d", year1, monthOfYear + 1, dayOfMonth1);

            }, year, month, dayOfMonth);

            // Show the DatePickerDialog
            datePickerDialog.show();
        }

        else if (v.getId() == rescheduleBtn.getId())
        {
            Intent intent = new Intent(WorkplaceTaskListActivity.this, AddUpdateTaskActivity.class);
            intent.putExtra("isAdd", false);
            activityLauncher.launch(intent);
        }
        else if (v.getId() == statusBtn.getId())
        {
            Task submitted_task;
            try {
                submitted_task = adapter.getSelectedItem();
            }
            catch (Exception e)
            {
                Toast.makeText(this, "Pick a task!", Toast.LENGTH_LONG).show();
                return;
            }

            try {
                Communicator.changeTaskStatus(this, username, submitted_task.getTask_name(), workplace_name, current_date);

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }
    //getter for the task list
    public ArrayList<Task> getTasks()
    {
        return this.tasks;
    }
}