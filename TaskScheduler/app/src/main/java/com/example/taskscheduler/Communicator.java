package com.example.taskscheduler;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * The Communicator class facilitates communication with a remote server for user authentication and task management.
 */
public class Communicator {
    private static final String SERVER_URL = "http://192.168.1.19:3000";
    private static final String SERVER_URL2 = "http://10.0.2.2:3000";
    private static final int SUCCESS_CODE = 200;

    /**
     * Attempts to sign up a new user with the provided username and password.
     * @param context The context of the calling activity or application.
     * @param username The username of the user to sign up.
     * @param password The password of the user to sign up.
     * @return true if the sign up is successful, false otherwise.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static boolean signUp(Context context, String username, String password) throws IOException {
        boolean success = true;
        //create connection to the server
        URL url = new URL(SERVER_URL + "/signup");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        String requestBody = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";

        //try sending information
        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        // Username already exists
        try
        {
            if (responseCode != SUCCESS_CODE)
            {

                // Display dialog with server's message
                String responseMessage = getResponseMessage(connection);
                throw new RuntimeException(responseMessage);
            }

        }
        catch(Exception e)
        {
            Utils.showDialog(context, "Username Already Taken");
            success = false;
        }

        connection.disconnect();
        return success;
    }

    /**
     * Attempts to log in an existing user with the provided username and password.
     * @param context The context of the calling activity or application.
     * @param username The username of the user to log in.
     * @param password The password of the user to log in.
     * @return true if the login is successful, false otherwise.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static boolean logIn(Context context,String username, String password) throws IOException {
        //create connection to the server
        boolean success = true;
        URL url = new URL(SERVER_URL + "/login");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        String requestBody = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";

        //try sending information
        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        // Username or password does not match
        try
        {
            if (responseCode != SUCCESS_CODE)
            {

                // Display dialog with server's message
                String responseMessage = getResponseMessage(connection);
                throw new RuntimeException(responseMessage);
            }

        }
        catch(Exception e)
        {
            Utils.showDialog(context, "Username Or Password Does Not Match");
            success = false;
        }
        connection.disconnect();
        return success;
    }

    /**
     * Creates a new workplace with the specified owner name, workplace name, and password.
     * @param context The context of the calling activity or application.
     * @param ownerName The name of the owner of the workplace.
     * @param workplaceName The name of the workplace to create.
     * @param password The password required to join the workplace.
     * @return true if the workplace creation is successful, false otherwise.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static boolean createWorkplace(Context context, String ownerName, String workplaceName, String password) throws IOException {
        boolean success = true;
        URL url = new URL(SERVER_URL + "/create_workplace");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        String requestBody = "{\"owner_name\": \"" + ownerName + "\", \"workplace_name\": \"" + workplaceName + "\", \"password\": \"" + password + "\"}";

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        try {
            if (responseCode != SUCCESS_CODE) {
                String responseMessage = getResponseMessage(connection);
                throw new RuntimeException(responseMessage);
            }
        } catch (Exception e) {
            Utils.showDialog(context, "Error creating workplace");
            success = false;
        }

        connection.disconnect();
        return success;
    }

    //method to join a workplace
    public static boolean joinWorkplace(Context context, String username, String workplaceName, String password) throws IOException {
        boolean success = true;
        URL url = new URL(SERVER_URL + "/join_workplace");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        String requestBody = "{\"username\": \"" + username + "\", \"workplace_name\": \"" + workplaceName + "\", \"password\": \"" + password + "\"}";

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        try {
            if (responseCode != SUCCESS_CODE) {
                String responseMessage = getResponseMessage(connection);
                throw new RuntimeException(responseMessage);
            }
        } catch (Exception e) {
            Utils.showDialog(context, "Error joining workplace");
            success = false;
        }

        connection.disconnect();
        return success;
    }


    /**
     * Retrieves a list of all workplaces available.
     * @param context The context of the calling activity or application.
     * @return An ArrayList of Workplace objects representing the available workplaces.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static ArrayList<Workplace> getWorkplaces(Context context) throws IOException {
        ArrayList<Workplace> workplaces = new ArrayList<>();
        URL url = new URL(SERVER_URL + "/get_workplaces");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json");

        int responseCode = connection.getResponseCode();

        // If request is successful, parse response and create Workplace objects
        if (responseCode == SUCCESS_CODE) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                // Parse JSON response to ArrayList<Workplace> using Gson
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<Workplace>>(){}.getType();
                String r = response.toString();
                workplaces = gson.fromJson(response.toString(), listType);

            }
        } else {
            // If request fails, show error message
            Utils.showDialog(context, "Error fetching workplaces");
        }

        connection.disconnect();
        return workplaces;
    }

    /**
     * Retrieves the name of the workplace associated with the specified user.
     * @param context The context of the calling activity or application.
     * @param username The username of the user whose workplace is to be retrieved.
     * @return The name of the user's workplace, or null if not found.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static String getUsersWorkplace(Context context, String username) throws IOException {
        String workplaceName = null;
        URL url = new URL(SERVER_URL + "/get_users_workplace");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        String requestBody = "{\"username\": \"" + username + "\"}";

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        if (responseCode == SUCCESS_CODE) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                // Parse JSON response using Gson
                Gson gson = new Gson();
                WorkplaceJsonResponse jsonResponse = gson.fromJson(br, WorkplaceJsonResponse.class);
                workplaceName = jsonResponse.getWorkplaceName();
            }
        } else {
            Utils.showDialog(context, "Error fetching user's workplace");
        }

        connection.disconnect();
        return workplaceName;
    }

    /**
     * Retrieves a list of tasks associated with the specified workplace.
     * @param context The context of the calling activity or application.
     * @param workplaceName The name of the workplace for which tasks are to be retrieved.
     * @return An ArrayList of Task objects representing the tasks associated with the workplace.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static ArrayList<Task> getTasksForWorkplace(Context context, String workplaceName, String date) throws IOException {
        ArrayList<Task> tasks = new ArrayList<>();
        URL url = new URL(SERVER_URL + "/get_tasks_for_workplace");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST"); // Change request method to POST
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        // Construct the request JSON with the workplaceName and date parameters
        String requestBody = "{\"workplace_name\": \"" + workplaceName + "\", \"date\": \"" + date + "\"}";

        // Write the request JSON to the connection's output stream
        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        // If request is successful, parse response and create Task objects
        if (responseCode == SUCCESS_CODE) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                // Parse JSON response to ArrayList<Task> using Gson
                Gson gson = new Gson();
                Type taskListType = new TypeToken<ArrayList<Task>>(){}.getType();
                String r = response.toString();
                tasks = gson.fromJson(response.toString(), taskListType);
            }
        } else {
            // If request fails, show error message
            Utils.showDialog(context, "Error fetching tasks");
        }

        connection.disconnect();
        return tasks;
    }



    /**
     * Adds a new task to the specified workplace.
     * @param context The context of the calling activity or application.
     * @param workplaceName The name of the workplace to which the task is to be added.
     * @param creatorName The name of the user creating the task.
     * @param startTime The start time of the task.
     * @param endTime The end time of the task.
     * @param name The description or name of the task.
     * @return true if the task addition is successful, false otherwise.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static boolean addTask(Context context, String workplaceName, String creatorName, String startTime, String endTime, String name, String date) throws IOException {
        boolean success = true;
        URL url = new URL(SERVER_URL + "/add_task");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        String requestBody = "{\"workplace_name\": \"" + workplaceName + "\", \"creator_name\": \"" + creatorName + "\", \"start_time\": \"" + startTime + "\", \"end_time\": \"" + endTime + "\", \"description\": \"" + name + "\", \"date\": \"" + date + "\"}";

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        try {
            if (responseCode != SUCCESS_CODE) {
                String responseMessage = getResponseMessage(connection);
                throw new RuntimeException(responseMessage);
            }
        } catch (Exception e) {
            Utils.showDialog(context, "Error adding task");
            success = false;
        }

        connection.disconnect();
        return success;
    }


    /**
     * Retrieves the response message from a HttpURLConnection.
     * @param connection The HttpURLConnection from which to retrieve the response message.
     * @return The response message as a String.
     */
    @NonNull
    private static String getResponseMessage(HttpURLConnection connection)
    {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("Response: " + response.toString());
            return response.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Deletes a task with the specified username, task name, and workplace name.
     * @param context The context of the calling activity or application.
     * @param username The username of the user attempting to delete the task.
     * @param taskName The name of the task to delete.
     * @param workplaceName The name of the workplace where the task belongs.
     * @return true if the task deletion is successful, false otherwise.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static boolean deleteTask(Context context, String username, String taskName, String workplaceName) throws IOException {
        boolean success = true;
        URL url = new URL(SERVER_URL + "/delete_task");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        String requestBody = "{\"username\": \"" + username + "\", \"task_name\": \"" + taskName + "\", \"workplace_name\": \"" + workplaceName + "\"}";

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        try {
            if (responseCode != SUCCESS_CODE) {
                String responseMessage = getResponseMessage(connection);
                throw new RuntimeException(responseMessage);
            }
        } catch (Exception e) {
            Utils.showDialog(context, "Error deleting task");
            success = false;
        }

        connection.disconnect();
        return success;
    }

    /**
     * Marks a task as done for the specified username and task name.
     * @param context The context of the calling activity or application.
     * @param username The username of the user marking the task as done.
     * @param taskName The name of the task to mark as done.
     * @return true if the task is successfully marked as done, false otherwise.
     * @throws IOException If an I/O error occurs while communicating with the server.
     */
    public static boolean changeTaskStatus(Context context, String username, String taskName, String workplaceName, String date) throws IOException {
        boolean success = true;
        URL url = new URL(SERVER_URL + "/change_task_status");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        String requestBody = "{\"task_name\": \"" + taskName + "\", \"username\": \"" + username + "\", \"workplace_name\": \"" + workplaceName + "\", \"date\": \"" + date + "\"}";

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = requestBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int responseCode = connection.getResponseCode();

        try {
            if (responseCode != SUCCESS_CODE) {
                String responseMessage = getResponseMessage(connection);
                throw new RuntimeException(responseMessage);
            }
        } catch (Exception e) {
            Utils.showDialog(context, "Error marking task as done");
            success = false;
        }

        connection.disconnect();
        return success;
    }


    /**
     * Inner class representing the JSON response when fetching a user's workplace.
     */
    class WorkplaceJsonResponse {
        private String workplaceName;

        /**
         * Gets the name of the user's workplace from the JSON response.
         * @return The name of the user's workplace.
         */
        public String getWorkplaceName() {
            return workplaceName;
        }
    }
}

