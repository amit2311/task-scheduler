package com.example.taskscheduler;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Adapter class for populating a RecyclerView with Task items.
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {
    private int row_index = -1;
    private ArrayList<Task> tasks;

    public TaskAdapter(ArrayList<Task> tasks){this.tasks = tasks;}

    /**
     * Updates the list of tasks and notifies the adapter of the change.
     * @param updatedTasks The updated list of tasks.
     */
    public void setTasks(ArrayList<Task> updatedTasks)
    {
        this.tasks = updatedTasks;
        Log.d("CODE_REACH", tasks.get(0).getStatus() + "");
        notifyDataSetChanged();
    }

    /**
     * Gets the selected task item.
     * @return The selected Task object.
     */
    public Task getSelectedItem()
    {
        return tasks.get(row_index);
    }

    /**
     * ViewHolder class to hold the views for individual task items.
     */
    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        public TextView TV_time;
        public TextView TV_taskName;
        public TextView TV_status;
        public LinearLayout background;
        public TaskViewHolder(@NonNull View itemView) {
            super(itemView);
            TV_time = itemView.findViewById(R.id.textViewTaskTime);
            TV_taskName = itemView.findViewById(R.id.textViewTaskName);
            TV_status = itemView.findViewById(R.id.textViewStatus);
            background = itemView.findViewById(R.id.workplace_item);
        }
    }

    /**
     * Creates a new TaskViewHolder instance.
     * @param parent The parent ViewGroup into which the new View will be added.
     * @param viewType The type of the new View.
     * @return A new TaskViewHolder instance.
     */
    @NonNull
    @Override
    public TaskAdapter.TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View taskView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_task, parent, false);
        return new TaskAdapter.TaskViewHolder(taskView);
    }

    /**
     * Binds data to the views inside the TaskViewHolder.
     * @param holder The TaskViewHolder instance to bind data to.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull TaskAdapter.TaskViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Task curr = tasks.get(position);
        holder.TV_taskName.setText(curr.getName());
        holder.TV_time.setText(curr.getTimeStr());

        holder.background.setOnClickListener(v -> {
            row_index = position;
            notifyDataSetChanged();
        });

        //paint selected item in blue
        if(position == row_index)
        {
            holder.background.setBackgroundColor(Color.BLUE);
        }
        else if (curr.getStatus())
        {
            holder.background.setBackgroundColor(Color.BLACK); // Set background color to black for pending tasks
        }
        else
        {
            holder.background.setBackgroundColor(Color.GREEN); // Set background color to green for done tasks
        }
        holder.TV_status.setText(curr.getStatus() ? "pending" : "done");
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     * @return The total number of items in the data set.
     */
    @Override
    public int getItemCount() {
        return tasks.size();
    }
}
