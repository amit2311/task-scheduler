package com.example.taskscheduler;

/**
 * Represents a workplace entity within the application.
 */
public class Workplace
{
    private String owner_name;
    private String name;

    public Workplace(String owner_name, String name)
    {
        this.name = name;
        this.owner_name = owner_name;
    }

    public String getOwner_name()
    {
        return this.owner_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }
}
