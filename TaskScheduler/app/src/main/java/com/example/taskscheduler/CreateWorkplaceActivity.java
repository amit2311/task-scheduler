package com.example.taskscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

public class CreateWorkplaceActivity extends AppCompatActivity implements View.OnClickListener {

    EditText WPname;
    EditText WPpassword;
    Button submitBtn;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_workplace);
        WPname = findViewById(R.id.workplace_name_input_create);
        WPpassword = findViewById(R.id.password_input_create);
        submitBtn = findViewById(R.id.submit_btn_create);
        submitBtn.setOnClickListener(this);
        username = getPassedUsername();
    }

    /**
     * Retrieves the username passed from the previous activity.
     * @return The username passed from the previous activity, or null if not found.
     */
    private String getPassedUsername()
    {
        Intent intent = getIntent();
        return intent.getStringExtra("username");
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == submitBtn.getId())
        {
            try {
                String workplace_name = WPname.getText().toString();
                String password = WPpassword.getText().toString();
                //check for password strength
                if(!Utils.checkPassword(password))
                {
                    Utils.showDialog(this, "password must contain capital letters, small letters, numbers, special characters, and be over 8 characters long");
                    return;
                }
                Communicator.createWorkplace(this, username, workplace_name, password);
                Intent intent = new Intent(this, WorkplaceTaskListActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("workplace_name", workplace_name);
                startActivity(intent);
            } catch (IOException e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}