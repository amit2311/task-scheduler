package com.example.taskscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    EditText usernameEntry;
    EditText passwordEntry;
    Button signUpBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        usernameEntry = findViewById(R.id.username_input_signup);
        passwordEntry = findViewById(R.id.password_input_signup);
        signUpBtn = findViewById(R.id.signup_btn_signup);
        signUpBtn.setOnClickListener(this);

        //made for creating connection safely with server
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == signUpBtn.getId())
        {
            String username = usernameEntry.getText().toString();
            String password = passwordEntry.getText().toString();

            //check for password strength
            if(!Utils.checkPassword(password))
            {
                Utils.showDialog(this, "password must contain capital letters, small letters, numbers, special characters, and be over 8 characters long");
                return;
            }
            try
            {
                if(Communicator.signUp(this, username, password))
                {
                    Intent intent = new Intent(this, WorkplaceMenuActivity.class);
                    intent.putExtra("username", username);
                    startActivity(intent);
                }
            }
            catch (IOException e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}