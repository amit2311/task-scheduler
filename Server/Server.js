const express = require('express');
const bodyParser = require('body-parser');
const dbManager = require('./db_manager');
const Utils = require('./Utils.js');
const schedule = require('node-schedule');
const app = express();
const PORT = 3000;

// Middleware
app.use(bodyParser.json());

// Signup endpoint
app.post('/signup', (req, res) => {
  
  const { username, password } = req.body;
  console.log("tried signup with" + username + password);
  dbManager.addUser(username, password, (err) => {
    if (err) {
      return res.status(500).json({ message: 'Error creating user' });
    }
    res.json({ message: 'User created successfully' });
  });
});

// Login endpoint
app.post('/login', (req, res) => {
  const { username, password } = req.body;
  console.log("tried login with" + username + password);
  dbManager.findUser(username, password, (err, user) => {
    if (err) {
      return res.status(401).json({ message: 'Invalid username or password' });
    }
    res.json({ message: 'Login successful' });
  });
});

app.post('/create_workplace', (req, res) => {
  const { owner_name, workplace_name, password } = req.body;
  
  // Get owner_id by owner_name
  dbManager.getUserIdByUsername(owner_name, (err, owner_id) => {
    if (err) {
      // Handle error
      res.status(500).send('Error finding user ID by username');
      return;
    }

    // Add workplace using obtained owner_id
    dbManager.addWorkplace(owner_id, workplace_name, password, (err, workplaceId) => {
      if (err) {
        // Handle error
        res.status(500).send('Error adding workplace');
        return;
      }
      
      console.log("created workplace " + workplace_name)
      // Success
      res.status(200).send(`Workplace created with ID: ${workplaceId}`);
    });
  });
});

// server endpoint to join a workplace
app.post('/join_workplace', (req, res) => {
  const { username, workplace_name, password } = req.body;
  
  dbManager.joinWorkplace(username, workplace_name, password, (err) => {
    if (err === 'Incorrect password') {
      return res.status(403).json({ message: 'Incorrect password' });
    } else if (err) {
      return res.status(500).json({ message: 'Error joining workplace' });
    }
    res.json({ message: 'Successfully joined workplace' });
  });
});


// Get workplaces endpoint
app.get('/get_workplaces', (req, res) => {
  dbManager.getWorkplaces((err, workplaces) => {
    if (err) {
      return res.status(500).json({ message: 'Error fetching workplaces' });
    }
    console.log(workplaces);
    res.json(workplaces);
  });
});

app.post('/get_users_workplace', (req, res) => {
  const { username } = req.body;

  dbManager.getUsersWorkplace(username, (err, workplaceName) => {
    if (err) {
      return res.status(500).json({ message: 'Error fetching user\'s workplace' });
    }
    res.json({ workplaceName });
  });
});

//TASK-related endpoints

// Get tasks for a specific workplace and date endpoint
app.post('/get_tasks_for_workplace', (req, res) => {
  const { workplace_name, date } = req.body;

  // Convert date to ISO format for comparison
  const isoDate = new Date(date).toISOString().split('T')[0];

  dbManager.getTasksForWorkplace(workplace_name, isoDate, (err, tasks) => {
    if (err) {
      return res.status(500).json({ message: 'Error fetching tasks for workplace' });
    }
    console.log(tasks);
    res.json(tasks);
  });
});

// Add task endpoint
app.post('/add_task', (req, res) => {
  const { workplace_name, creator_name, start_time, end_time, description, date } = req.body;

  // Check if start time is before end time
  if (start_time >= end_time) {
    return res.status(400).json({ message: 'Start time must be before end time' });
  }

  // Check for overlapping tasks
  dbManager.getTasksForWorkplace(workplace_name, date, (err, tasks) => {
    if (err) {
      return res.status(500).json({ message: 'Error fetching tasks for workplace' });
    }

    // Convert HH:MM format to minutes for easier comparison
    const startTimeInMinutes = Utils.getTimeInMinutes(start_time);
    const endTimeInMinutes = Utils.getTimeInMinutes(end_time);

    // Check for overlapping tasks
    const overlap = tasks.some(task => {
      const taskStartTime = Utils.getTimeInMinutes(task.start_time);
      const taskEndTime = Utils.getTimeInMinutes(task.end_time);
      return Utils.isOverlap(startTimeInMinutes, endTimeInMinutes, taskStartTime, taskEndTime);
    });

    if (overlap) {
      return res.status(400).json({ message: 'Task overlaps with existing task' });
    }

    // Add task to database
    dbManager.addTask(workplace_name, creator_name, description, start_time, end_time, date, (err) => {
      if (err) {
        console.log(err);
        return res.status(500).json({ message: 'Error adding task' });
      }
      res.json({ message: 'Task added successfully' });
    });
  });
});

    // Delete task endpoint
app.post('/delete_task', (req, res) => {
  const { username, task_name, workplace_name } = req.body;
  
  dbManager.deleteTaskByNameAndWorkplace(username, task_name, workplace_name, (err) => {
    if (err) {
      return res.status(500).json({ message: 'Error deleting task' });
    }
    res.json({ message: 'Task deleted successfully' });
  });
});

// Mark task as done endpoint
app.post('/change_task_status', (req, res) => {
  const { task_name, username, workplace_name } = req.body;
  
  dbManager.changeTaskStatus(task_name, username, workplace_name, (err) => {
    if (err) {
      return res.status(500).json({ message: err });
    }
    res.json({ message: 'Task marked as done successfully' });
  });
});



// Delete all tasks at 6:40 PM every day
const deleteAllTasksJob = schedule.scheduleJob('0 0 * * *', function() {
  // Delete all tasks
  dbManager.deleteYesterdayTasks(function(err) {
      if (err) {
          console.error('Error deleting tasks:', err);
      } else {
          console.log('All tasks deleted successfully');
      }
  });
});


// Start server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
