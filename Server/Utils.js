const Utils = {
    sortByTime: function(tasks) {
        // Sort tasks by start_time
        tasks.sort((task1, task2) => {
            const startTime1 = this.getTimeInMinutes(task1.start_time);
            const startTime2 = this.getTimeInMinutes(task2.start_time);
            return startTime1 - startTime2;
        });
        return tasks;
    },
    
    // Helper function to convert time string (HH:MM) to minutes
    getTimeInMinutes: function(timeString) {
        const [hours, minutes] = timeString.split(':');
        return parseInt(hours) * 60 + parseInt(minutes);
    },

    // Function to check if two time intervals overlap
    isOverlap: function(start1, end1, start2, end2) {
        return start1 < end2 && start2 < end1;
    }
};

module.exports = Utils;