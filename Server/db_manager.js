const sqlite3 = require('sqlite3').verbose();
const Utils = require('./Utils.js');
const dbPath = './mydatabase.db';

// Create SQLite database and table
const db = new sqlite3.Database(dbPath);

db.serialize(() => {
  db.run("CREATE TABLE IF NOT EXISTS USERS (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT, workplace_id INTEGER, FOREIGN KEY (workplace_id) REFERENCES workplaces(id))");
  db.run("CREATE TABLE IF NOT EXISTS WORKPLACES (id INTEGER PRIMARY KEY AUTOINCREMENT, owner_id INTEGER, name TEXT, password TEXT, FOREIGN KEY (owner_id) REFERENCES users(id))");
  db.run("CREATE TABLE IF NOT EXISTS TASKS (id INTEGER PRIMARY KEY AUTOINCREMENT, creator_id INTEGER, workplace_id INTEGER, task_name TEXT, start_time TEXT, end_time TEXT, status BOOLEAN, task_date TEXT, FOREIGN KEY (creator_id) REFERENCES users(id), FOREIGN KEY (workplace_id) REFERENCES workplaces(id))");
});



module.exports = {
  addUser: function(username, password, callback) {
    db.get("SELECT * FROM users WHERE username = ?", [username], (err, user) => {
      if (err || user) {
        callback(err || 'Username already exists');
      } else {
        db.run("INSERT INTO users (username, password) VALUES (?, ?)", [username, password], (err) => {
          if (err) {
            callback(err);
          } else {
            callback(null);
          }
        });
      }
    });
  },
  findUser: function(username, password, callback) {
    db.get("SELECT * FROM users WHERE username = ? AND password = ?", [username, password], (err, user) => {
      if (err || !user) {
        callback(err || 'Invalid username or password', null);
      } else {
        callback(null, user);
      }
    });
  },
  addWorkplace: function(owner_id, name, password, callback) {
    db.run("INSERT INTO WORKPLACES (owner_id, name, password) VALUES (?, ?, ?)", [owner_id, name, password], function(err) {
      if (err) {
        callback(err);
      } else {
        callback(null, this.lastID); // this.lastID contains the automatically generated ID
      }
    });
  },
   // Get workplaces function
   getWorkplaces: function(callback) {
    const query = "SELECT w.name AS workplace_name, u.username AS owner_name " +
                  "FROM WORKPLACES w " +
                  "INNER JOIN USERS u ON w.owner_id = u.id";

    db.all(query, [], (err, rows) => {
      if (err) {
        callback(err, null);
        return;
      }
      // Map rows to Workplace objects
      const workplaces = rows.map(row => ({
        owner_name: row.owner_name,
        name: row.workplace_name
      }));
      callback(null, workplaces);
    });
  },
  joinWorkplace: function(username, workplaceName, password, callback) {
    // First, find the workplace ID by workplace name
    db.get("SELECT id, password FROM workplaces WHERE name = ?", [workplaceName], (err, workplaceRow) => {
        if (err || !workplaceRow) {
            callback(err || 'Workplace not found');
            return;
        }
        
        const workplaceId = workplaceRow.id;
        const storedPassword = workplaceRow.password;
        
        // Verify if the provided password matches the stored password
        if (password !== storedPassword) {
            callback('Incorrect password');
            return;
        }
      
        // Update the user's workplace_id in the users table
        db.run("UPDATE users SET workplace_id = ? WHERE username = ?", [workplaceId, username], (err) => {
            if (err) {
                callback(err);
            } else {
                callback(null); // Success
            }
        });
    });
  },

  getUsersWorkplace: function(username, callback) {
    // First, get the workplace ID associated with the user
    db.get("SELECT workplace_id FROM users WHERE username = ?", [username], (err, userRow) => {
      if (err || !userRow) {
        callback(err || 'User not found');
        return;
      }
      
      const workplaceId = userRow.workplace_id;
      
      // Now, retrieve the workplace name using the obtained workplace ID
      db.get("SELECT name FROM workplaces WHERE id = ?", [workplaceId], (err, workplaceRow) => {
        if (err || !workplaceRow) {
          callback(err || 'Workplace not found');
          return;
        }
        
        const workplaceName = workplaceRow.name;
        
        callback(null, workplaceName); // Return the workplace name
      });
    });
  },
  
  //TASK-RELATED queries
  addTask: function(workplace_name, creator_name, task_name, start_time, end_time, task_date, callback) {
    // Get workplace_id by workplace_name
    db.get("SELECT id FROM WORKPLACES WHERE name = ?", [workplace_name], (err, workplace) => {
        if (err || !workplace) {
            callback(err || 'Workplace not found');
            return;
        }
        const workplace_id = workplace.id;

        // Get creator_id by creator_name
        db.get("SELECT id FROM USERS WHERE username = ?", [creator_name], (err, creator) => {
            if (err || !creator) {
                callback(err || 'Creator not found');
                return;
            }
            const creator_id = creator.id;

            // Check for overlapping tasks on the same date
            const overlapQuery = "SELECT * FROM TASKS WHERE workplace_id = ? AND task_date = ?";
            db.all(overlapQuery, [workplace_id, task_date], (err, tasks) => {
                if (err) {
                    callback(err);
                    return;
                }

                // Convert start_time and end_time to minutes for comparison
                const startTimeInMinutes = Utils.getTimeInMinutes(start_time);
                const endTimeInMinutes = Utils.getTimeInMinutes(end_time);

                // Check for overlapping tasks
                const overlap = tasks.some(task => {
                    const taskStartTime = Utils.getTimeInMinutes(task.start_time);
                    const taskEndTime = Utils.getTimeInMinutes(task.end_time);
                    return Utils.isOverlap(startTimeInMinutes, endTimeInMinutes, taskStartTime, taskEndTime);
                });

                if (overlap) {
                    callback("Overlap detected with existing tasks on the same date");
                    return;
                }

                // No overlap, proceed to add the task
                const insertQuery = "INSERT INTO TASKS (workplace_id, creator_id, task_name, start_time, end_time, status, task_date) VALUES (?, ?, ?, ?, ?, true, ?)";
                db.run(insertQuery, [workplace_id, creator_id, task_name, start_time, end_time, task_date], function(err) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, this.lastID); // Success
                    }
                });
            });
        });
    });
  },

  changeTaskStatus: function(task_name, user_name, workplace_name, callback) {
    // Get user's ID by username
    db.get("SELECT id FROM USERS WHERE username = ?", [user_name], (err, user) => {
        if (err || !user) {
            callback(err || 'User not found');
            return;
        }
        const user_id = user.id;

        // Get creator_id by task_name and workplace_name
        db.get("SELECT creator_id FROM TASKS WHERE task_name = ? AND workplace_id = (SELECT id FROM WORKPLACES WHERE name = ?)", [task_name, workplace_name], (err, task) => {
            if (err || !task) {
                callback(err || 'Task not found');
                return;
            }
            const creator_id = task.creator_id;

            // Check if the user is the creator of the task
            if (user_id === creator_id) {
                // User is the creator, update the status of the task to true
                db.run("UPDATE TASKS SET status = NOT status WHERE task_name = ? AND workplace_id = (SELECT id FROM WORKPLACES WHERE name = ?)", [task_name, workplace_name], function(err) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null); // Success
                    }
                });
            } else {
                callback('You do not have permission to mark this task as done');
            }
        });
    });
  },


  // Get tasks for a specific workplace
  getTasksForWorkplace: function(workplace_name, date, callback) {
    // Get workplace_id by workplace_name
    db.get("SELECT id FROM WORKPLACES WHERE name = ?", [workplace_name], (err, workplace) => {
        if (err || !workplace) {
            callback(err || 'Workplace not found', null);
            return;
        }
        const workplace_id = workplace.id;

        const query = "SELECT task_name, start_time, end_time, status FROM TASKS WHERE workplace_id = ? AND task_date = ?";

        db.all(query, [workplace_id, date], (err, rows) => {
            if (err) {
                callback(err, null);
                return;
            }
            // Modify each task object to include status as true/false
            const tasksWithStatus = rows.map(task => {
                return {
                    task_name: task.task_name,
                    start_time: task.start_time,
                    end_time: task.end_time,
                    status: task.status === 1 // Convert 1 to true, 0 to false
                };
            });
            // Sort tasks by time
            const sortedTasks = Utils.sortByTime(tasksWithStatus);
            callback(null, sortedTasks);
        });
    });
  },

  deleteYesterdayTasks: function(callback) {
    // Calculate yesterday's date
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    const formattedDate = yesterday.toISOString().split('T')[0]; // Format date as 'YYYY-MM-DD'

    // Delete tasks from yesterday from the TASKS table
    db.run("DELETE FROM TASKS WHERE task_date = ?", [formattedDate], function(err) {
        if (err) {
            callback(err);
        } else {
            callback(null); // Success
        }
    });
},

  // Delete a task
  deleteTaskByNameAndWorkplace: function(username, task_name, workplace_name, callback) {
    // Get user's ID by username
    db.get("SELECT id FROM USERS WHERE username = ?", [username], (err, user) => {
      if (err || !user) {
        callback(err || 'User not found');
        return;
      }
      const user_id = user.id;

      // Get workplace_id by workplace_name
      db.get("SELECT id, owner_id FROM WORKPLACES WHERE name = ?", [workplace_name], (err, workplace) => {
        if (err || !workplace) {
          callback(err || 'Workplace not found');
          return;
        }
        const workplace_id = workplace.id;
        const owner_id = workplace.owner_id;

        // Check if the user is the owner of the workplace or if the task belongs to the user
        if (user_id === owner_id) {
          // User is the owner, allow deletion
          db.run("DELETE FROM TASKS WHERE task_name = ? AND workplace_id = ?", [task_name, workplace_id], function(err) {
            if (err) {
              callback(err);
            } else {
              callback(null); // Success
            }
          });
        } else {
          // User is not the owner, check if the task belongs to the user
          db.get("SELECT id FROM TASKS WHERE task_name = ? AND workplace_id = ? AND creator_id = ?", [task_name, workplace_id, user_id], (err, task) => {
            if (err || !task) {
              callback(err || 'Task not found or you do not have permission to delete it');
              return;
            }
            // Task belongs to the user, allow deletion
            db.run("DELETE FROM TASKS WHERE task_name = ? AND workplace_id = ?", [task_name, workplace_id], function(err) {
              if (err) {
                callback(err);
              } else {
                callback(null); // Success
              }
            });
          });
        }
      });
    });
  },

  getUserIdByUsername: function(username, callback) {
    db.get("SELECT id FROM users WHERE username = ?", [username], (err, row) => {
      if (err || !row) {
        callback(err || 'User not found');
      } else {
        callback(null, row.id);
      }
    });
  },
  getWorkplaceIdByName: function(workplaceName, callback) {
    db.get("SELECT id FROM WORKPLACES WHERE name = ?", [workplaceName], (err, row) => {
      if (err || !row) {
        callback(err || 'Workplace not found');
      } else {
        callback(null, row.id);
      }
    });
  }

};

